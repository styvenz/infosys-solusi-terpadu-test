import { useEffect, useState } from "react";
import "./Table.css";

const HeaderCell = ({ column, sorting, sortTable }) => {
  return (
    <th
      key={column}
      className="films-table-cell"
    >
      {column}
    </th>
  );
};

const Header = ({ columns }) => {
  return (
    <thead>
      <tr>
        {columns.map((column) => (
          <HeaderCell
			key={column}
            column={column}
          />
        ))}
      </tr>
    </thead>
  );
};

const Content = ({ entries, columns }) => {
  return (
    <tbody>
      {entries.map((entry) => (
        <tr key={entry.episode_id}>
          {columns.map((column) => (
            <td key={column} className="films-table-cell">
              {entry[column]}
            </td>
          ))}
        </tr>
      ))}
    </tbody>
  );
};

const SearchBar = ({ searchTable }) => {
  const [searchValue, setSearchValue] = useState("");
  const submitForm = (e) => {
    e.preventDefault();
    searchTable(searchValue);
  };
  return (
    <div className="search-bar">
      <form onSubmit={submitForm}>
        <input
          type="text"
          placeholder="Search..."
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
        />
      </form>
    </div>
  );
};

const Table = () => {
  const [films, setFilms] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const columns = ["episode_id", "title", "director"];
  const searchTable = (newSearchValue) => {
    setSearchValue(newSearchValue);
  };

  const [loading, setLoading] = useState(false);

  const [page, setPage] = useState(1);
  const itemsPerPage = 2;
  const startIndex = (page - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentItems = films.slice(startIndex, endIndex);

  useEffect(() => {
    setLoading(true);
	const url = `https://swapi.dev/api/films/?search=${searchValue}`;
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        setFilms(res.results);
		setLoading(false);
      });
  }, [searchValue]);
  
  const previous = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const next = () => {
    const totalPages = Math.ceil(films.length / itemsPerPage);
    if (page < totalPages) {
      setPage(page + 1);
    }
  };


  return (
    <div>
      <SearchBar searchTable={searchTable} />
	  {!loading ? 
		<>
		  <table className="films-table">
			<Header columns={columns} />
			<Content entries={currentItems} columns={columns} />
		  </table>
		  <button onClick={() => previous()}> Prev Page </button>
		  <button onClick={() => next()}> Next Page </button>
		</>
	  : <div> Loading... </div> }
    </div>
  );
};

export default Table;